export class PowerPlant {
  constructor() {
    this.working = true;
    this.connectedHouseholds = [];
  }

  connectHousehold(household) {
    this.connectedHouseholds.push(household)
  }

  kill() {
    this.working = false;
    this.actualizeHouseholdsStatus();
  }

  repair() {
    this.working = true;
    this.actualizeHouseholdsStatus();
  }

  isAlive() {
    return this.working;
  }

  actualizeHouseholdsStatus() {
    this.connectedHouseholds.forEach(hh => hh.actualizeStatus());
  }
}

/**
 * This class is just a facade for your implementation, the tests below are using the `World` class only.
 * Feel free to add the data and behavior, but don't change the public interface.
 */

import { Household } from "./Household.js"
import { PowerPlant } from "./PowerPlant.js"

export class World {
  constructor() {
    this.network = {
      powerPlants: [],
      houseHolds: [],
    };
  }

  createPowerPlant() {
    const powerPlant = new PowerPlant();
    this.network.powerPlants.push(powerPlant);

    return powerPlant;
  }

  createHousehold() {
    const household = new Household();
    this.network.houseHolds.push(household);

    return household;
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    household.connectPowerPlant(powerPlant);
  }

  connectHouseholdToHousehold(household1, household2) {
    household1.connectHousehold(household2);
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    household.disconnectPowerPlant(powerPlant);
  }

  killPowerPlant(powerPlant) {
    powerPlant.kill();

    if(!this.hasNetworkElectricity()) {
      this.network.houseHolds.forEach(hh => hh.setElectricity(false));
    }
  }

  hasNetworkElectricity() {
    return !!this.network.powerPlants.find(pp => pp.isAlive() === true);
  }

  repairPowerPlant(powerPlant) {
    powerPlant.repair();
  }

  householdHasEletricity(household) {
    return household.hasElectricity();
  }
}

export class Household {
  constructor() {
    this.electricity = false;
    this.connectedPowerPlants = [];
    this.connectedHouseholds = [];
  }

  setElectricity(value) {
    if(typeof value !== 'boolean') {
      return;
    }

    this.electricity = value;
  }

  connectPowerPlant(powerPlant) {
    const isPPAlreadyConnected = this.connectedPowerPlants.includes(powerPlant);
    //dont provide connection if array contains this power plant
    if(isPPAlreadyConnected) {
      return
    }

    this.connectedPowerPlants.push(powerPlant);
    powerPlant.connectHousehold(this);
    this.actualizeStatus();
  }

  connectHousehold(household) {
    //dont provide connection if (this) is the same household
    if (this === household) {
      return
    }

    const isHHAlreadyConnected = this.connectedHouseholds.includes(household);
    //dont provide connection if array contains this household
    if(isHHAlreadyConnected) {
      return
    }

    this.connectedHouseholds.push(household);
    household.connectHousehold(this);
    this.actualizeStatus();
  }

  disconnectPowerPlant(powerPlant) {
    this.connectedPowerPlants = this
      .connectedPowerPlants
      .filter(pp => pp !== powerPlant);

    this.actualizeStatus();
  }

  actualizeStatus() {
    //return if not connected
    if(!this.isConnected()) {
      this.electricity = false;
      return
    }

    const electricityFromHH = this
      .connectedHouseholds
      .find(hh => hh.hasElectricity() === true);

    const electricityFromPP = this
      .connectedPowerPlants
      .find(pp => pp.isAlive() === true);

    const prevElectricity = this.electricity;
    this.electricity = !!(electricityFromHH || electricityFromPP);

    if(prevElectricity !== this.electricity) {
      this.connectedHouseholds.forEach(hh => hh.actualizeStatus());
    }
  }

  //check for any connections
  isConnected() {
    const connectionsCount = this.connectedPowerPlants.length
      + this.connectedHouseholds.length;

    return !!connectionsCount;
  }

  hasElectricity() {
    return this.electricity;
  }
}
